import React, {Component, CSSProperties} from 'react';
import { uuidv4 } from './common/Functions';
import { Store } from './common/Store';
import { DataRequest } from './Server';

interface DocumentProps {
    forceLogout: any
}

interface DocumentState {
	isCardElectronic: boolean,
	idCard: FileList | null;
    addressProof: FileList | null;
    visible: boolean;
}

export class DocumentSubmission extends Component<DocumentProps, DocumentState> {

    constructor(props: DocumentProps){
        super(props);   

        this.state = {
            isCardElectronic: true,
            idCard: null,
            addressProof: null,
            visible: false
        }
    }

    setIfCardIsElectronic = (e: React.FormEvent<HTMLInputElement>) => {
        const value : boolean = (e.currentTarget.value.toLowerCase() === "true");
        this.setState({ isCardElectronic: value });
    }

    setFile(fileList: FileList | null) {
        var value : Blob | string = ""
        if(fileList !== undefined){
            if(fileList !== null){
                if(fileList.length > 0){
                    value = fileList[0];
                }
            }
        }

        return value;
    }

    setIDCard = (e: React.FormEvent<HTMLInputElement>) => {
        const value : FileList | null = e.currentTarget.files;
        this.setState({ idCard: value});
    }

    setAddressProof = (e: React.FormEvent<HTMLInputElement>) => {
        const value : FileList | null = e.currentTarget.files;
        this.setState({ addressProof: value});
    }

    setVisibility = () => {
        this.setState({visible: !this.state.visible});
    }

    cancel = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.setVisibility();
    }

    submit = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        const data = new FormData();
        const addressProof = this.state.addressProof? this.state.addressProof[0] : "null";
        const idCard = this.state.idCard? this.state.idCard[0] : "null";
        data.append("addressProof", addressProof);
        data.append("idCard", idCard);

        fetch(DataRequest.requestDocumentUpload(Store.getItem(Store.customerIdKey), (this.state.isCardElectronic)? "YES" : "NO").url, {
            method: 'POST',
            body: data
        })
        .then(res => res.text())
        .then(
            (result) => {
                alert(result);
                this.setVisibility();
                this.props.forceLogout();
            }
        )
        .catch(error => {
            alert("Failed to upload");
            console.log(error);
        })
    }

    render() {
        var collapseStyle: CSSProperties = {};
        const key: string = Store.getItem(Store.connectionId) || "none";

        if(this.state.visible)
        {
            collapseStyle = {
                height: "auto",
                overflow: "hidden",
                margin: 0
            }
        } else {
            collapseStyle = {
                height: "0px",
                overflow: "hidden",
                margin: 0
            }
        }

        return (
            <div>
                <a onClick={this.setVisibility}>UPLOAD FILE {(this.state.visible)?"↑":"↓"}</a>
                <div className="formContainer" style={collapseStyle}>
                    <h3>Please upload your documents​</h3>
                    <form>
                        <label>Upload ID Card</label><input type="file" accept=".pdf" onChange={this.setIDCard}/>
                        <label>Is ID Card Electronic?</label>
                            <span className="radioGroup"><input type="radio" id="is-electronic" name="isCardElectronic" value="true" onChange={this.setIfCardIsElectronic} checked={this.state.isCardElectronic === true}/><label htmlFor="is-electronic" className="value">Yes</label></span>
                            <span className="radioGroup"><input type="radio" id="is-not-electronic" name="isCardElectronic" value="false" onChange={this.setIfCardIsElectronic} checked={this.state.isCardElectronic === false}/><label htmlFor="is-not-electronic" className="value">No</label></span>
                        <label>Upload Proof Of Address​</label><input type="file" accept=".pdf" onChange={this.setAddressProof}/>
                        <input type="submit" className="cancel-button" value="Cancel" onClick={this.cancel}/>
                        <input type="submit" value="Submit" onClick={this.submit}/>
                    </form>
                </div>
                {key}
            </div>
        );
    }
}