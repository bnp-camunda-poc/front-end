import { uuidv4 } from "./common/Functions";
import { Store } from "./common/Store";

export class DataRequest{
    url: string

    static registerConnectionId = (registerNew : boolean = false):string => {
        var uuid : string = Store.getItem(Store.connectionId) || uuidv4();
    
        if(registerNew) uuid = uuidv4();

        Store.setItem(Store.connectionId, uuid);

        return uuid;
    }

    static requestLogin = (firstName: string, lastName: string, dateOfBirth: string):DataRequest => {
        const id = DataRequest.registerConnectionId();
        return new DataRequest((process.env.REACT_APP_LOGIN_URI || "") + `/?firstName=${firstName}&familyName=${lastName}&dob=${dateOfBirth}&id=${id}`);
    }
    static requestRegistration = ():DataRequest => {
        const id = DataRequest.registerConnectionId();
        return new DataRequest((process.env.REACT_APP_REGISTRATION_URI || "") + `?id=${id}`);
    }
    static requestDocumentUpload = (customerID: string | null, isElectronic: string):DataRequest => {
        const id = DataRequest.registerConnectionId();
        return new DataRequest((process.env.REACT_APP_DOCUMENTUPLOAD_URI || "") + `?customerId=${customerID}&isElectronic=${isElectronic}&id=${id}`);
    }

    constructor(uri: string){
        const serverURL: string | undefined = process.env.REACT_APP_DATASERVER;
        const versionURI: string | undefined = process.env.REACT_APP_VERSION;
        this.url = (serverURL === undefined)? uri : serverURL + versionURI + uri; 

        console.log(serverURL);
        console.log(this.url);
    }
}