import React, {Component} from 'react';
import './styles/App.css';
import Overview from './Overview';

export default class App extends Component<{}> {

render() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="App-body">
          <Overview />
        </div>
      </header>
    </div>
  );
}
}