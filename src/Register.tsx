import React, {ChangeEvent, Component} from 'react';
import moment from 'moment';
import  { Credentials, Nationality, Gender, Language, IDCard, ResidenceStatus } from './common/Types';
import { UserRegistration } from './common/User';
import { DataRequest } from './Server';
import { Store } from './common/Store';
import { User } from './common/User';
import { uuidv4 } from './common/Functions';

export interface RegistationProps {
    credentials: Credentials,
    visibility: boolean,
    setVisibility: any,
    setLoginStatus: any
}

interface DisplayState {
    checking: boolean
}

interface RegistrationState {
    credentials: Credentials,
    email: string,
    nationality: Nationality,
    gender: Gender,
    language: Language,
    address: string,
    idCard: IDCard,
    residenceStatus : ResidenceStatus
}

export class Register extends Component<RegistationProps, RegistrationState> {

    displayState : DisplayState = {
        checking: false
    }
    constructor(props: RegistationProps){
        super(props);
        this.state = {
                credentials: props.credentials,
                email: "",
                nationality: Nationality.nationalities[0],
                gender: Gender.genders[0],
                language: Language.languages[0],
                address: '',
                idCard: new IDCard('', new Date()),
                residenceStatus: ResidenceStatus.residenceStatusOptions[0]
        }
    }

    setFirstName = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        var updateCredentials = this.state.credentials;
        updateCredentials.firstName = value;

        this.setState({ credentials: updateCredentials })
    }

    setLastName = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        var updateCredentials = this.state.credentials;
        updateCredentials.lastName = value;

        this.setState({ credentials: updateCredentials })
    }

    setDateOfBirth = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.valueAsDate;
        var updateCredentials = this.state.credentials;
        updateCredentials.dateOfBirth = value;

        this.setState({ credentials: updateCredentials })
    }

    setEmail = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;

        this.setState({ email: value })
    }

    setAddress = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;

        this.setState({ address: value })
    }

    setIDCardNumber = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        var updateIdCard = this.state.idCard;
        updateIdCard.idCardNumber = value;

        this.setState({ idCard: updateIdCard })
    }

    setIDCardValidity = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.valueAsDate;
        var updateIdCard = this.state.idCard;
        updateIdCard.idCardValidity = value;

        this.setState({ idCard: updateIdCard })
    }

    setGender = (gender: Gender) => {
        console.log(`new gender: ${gender.value}`);
        this.setState({gender: gender})
    }

    setNationality = (e: ChangeEvent<HTMLSelectElement>) => {
        const nationality : Nationality = Nationality.find(e.currentTarget.value);
        console.log(`new nationality: ${nationality.value}`);
        this.setState({nationality: nationality})
    }

    setResidenceStatus = (e: ChangeEvent<HTMLSelectElement>) => {
        var residenceStatus : ResidenceStatus = ResidenceStatus.find(e.currentTarget.value);
        console.log(`new residenceStatus: ${residenceStatus.value}`);
        this.setState({residenceStatus: residenceStatus})
    }

    setLanguage = (language: Language) => {
        console.log(`new language: ${language.value}`);
        this.setState({language: language})
    }

    doRegistration = (response: Response) => {
        response.json().then(data => {
            const user : User = data;
            var registrationStatus : boolean = false;
            const checkStatus : string = (process.env.REACT_APP_REGISTRATION_STATUS || "true");
            if(user.status.toUpperCase() === checkStatus.toUpperCase()){
                Store.setItem(Store.customerIdKey, user.customerId);
                alert(`${this.state.credentials.firstName} ${this.state.credentials.lastName}, you're now registered`);
                registrationStatus = true;
            } else {
                alert("Failed to register");
                this.displayState.checking = false;
                console.log(data.error);
            }
            
            this.props.setLoginStatus(registrationStatus);
        });
    }

    checkRegistration = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.displayState.checking = true;
        this.forceUpdate();

        const newRegistration : UserRegistration = new UserRegistration(
            this.state.credentials,
            this.state.email,
            this.state.nationality,
            this.state.gender,
            this.state.language,
            this.state.address,
            this.state.idCard,
            this.state.residenceStatus
        );
        const strNewRegistation : string = JSON.stringify(newRegistration);

        fetch(DataRequest.requestRegistration().url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: strNewRegistation
        })
        .then(response => this.doRegistration(response))
        .catch(error => {
            alert("Failed to register");
            this.displayState.checking = false;
            console.log(error);
        });
    }

    cancelRegistration = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.props.setVisibility(false);
    }

    render() {
        const { firstName, lastName, dateOfBirth } = this.state.credentials;
        const email = this.state.email;
        const address = this.state.address;
        const { idCardNumber, idCardValidity } = this.state.idCard;
        const registrationDisplay = {
            display: (this.displayState.checking)? 'none' : 'block'
        }
        const processingRegistration = {
            display: (this.displayState.checking)? 'block' : 'none'
        }
        
        return (
            <div>
                <div className="formContainer" style={registrationDisplay}>
                    <h2>Register</h2>
                    <form >
                        <label>First Name</label><input type="text" placeholder="First Name" value={firstName} onChange={this.setFirstName}/>
                        
                        <label>Last Name</label><input type="text" placeholder="Last Name" value={lastName} onChange={this.setLastName}/>
                        
                        <label>Date of Birth</label><input type="date" value={ moment(dateOfBirth).format('YYYY-MM-DD') } onChange={this.setDateOfBirth}/>

                        <label>Email</label><input type="text" placeholder="email@example.com" value={email} onChange={this.setEmail}/>
                        
                        <label>Nationality</label><select multiple={false} placeholder="Select a nationality" onChange={this.setNationality}>
                            {
                                Nationality.nationalities.map(nationality => {
                                    return <option>{nationality.value}</option>
                                })
                            }
                        </select>
                        
                        <label>Gender</label>
                        {
                            Gender.genders.map(gender => {
                                return <span className="radioGroup"><input key={gender.value} type="radio" id={gender.value} name="gender" value={gender.value} onChange={(e: React.FormEvent<HTMLInputElement>) => {this.setGender(gender)}}/><label htmlFor={gender.value} className="value">{gender.value}</label></span>
                            })
                        }
                        
                        <label>Language</label>
                        {
                            Language.languages.map(language => {
                                return <span className="radioGroup"><input key={language.value} type="radio" id={language.value} name="language" value={language.value} onChange={(e: React.FormEvent<HTMLInputElement>) => {this.setLanguage(language)}}/><label htmlFor={language.value} className="value">{language.value}</label></span>
                            })
                        }

                        <label>Address</label><input type="text" placeholder="Street-name Street-No, Postal-code City/Municipality" value={address} onChange={this.setAddress}/>
                        
                        <label>ID Card Number</label><input type="text" placeholder="Personal ID Card Number" value={idCardNumber} onChange={this.setIDCardNumber}/>
                        
                        <label>ID Card validity</label><input type="date" onKeyDown={(e) => { e.preventDefault(); return false; }} value={ moment(idCardValidity).format('YYYY-MM-DD') } onChange={this.setIDCardValidity}/>
                        
                        <label>Residence status</label><select multiple={false} placeholder="Select residency status" onChange={this.setResidenceStatus}>
                            {
                                ResidenceStatus.residenceStatusOptions.map(status => {
                                    return <option>{status.value}</option>
                                })
                            }
                        </select>
                        
                        <input type="submit" className="cancel-button" value="Cancel" onClick={this.cancelRegistration}/>
                        <input type="submit" value="Save" onClick={this.checkRegistration}/>
                    </form>
                </div>
                <div style={processingRegistration}>
                    Processing Registration
                </div>
            </div>
        )
    }

}

