import React, {Component} from 'react';
import { Store } from './common/Store';
import { DocumentSubmission } from './DocumentSubmission';
import { Login } from './Login';

interface OverviewState{
    loggedOn : boolean
}

export default class Overview extends Component<{}, OverviewState> {
    
    constructor(props: any){
        super(props);
        this.state = {
            loggedOn : (Store.getItem(Store.customerIdKey) !== null)? true : false
        }
    }

    setLoginStatus = (status: boolean) =>{
        this.setState({loggedOn : status});
        this.forceUpdate();
    }

    Logout = () =>{
        Store.removeItem(Store.customerIdKey);
        this.setState({loggedOn : false});
    }

    render() {
        const loggedOn : boolean = this.state.loggedOn;

        const customerId : string = Store.getItem(Store.customerIdKey) || "none";

        if(loggedOn){
            return (
                <div>
                    <h1>Welcome, Customer {customerId}</h1>
                    <a className="logout-button" onClick={this.Logout}>Logout</a>
                    <DocumentSubmission forceLogout={this.Logout} />
                </div>
            );
        } else {
            return (
                <div>
                    <Login loggedOn={this.state.loggedOn} setLoginStatus={this.setLoginStatus}/>
                </div>
            );
        }
    }
}