import React from 'react';

export class Credentials {
    firstName: string
    lastName: string
    dateOfBirth: Date | null

    constructor(firstName: string, lastName: string, dateOfBirth: Date | null = null) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }
}

export class Nationality {
    value: string

    static nationalities: Nationality[] = [
        new Nationality("Belgium"),
        new Nationality("Dutch"),
        new Nationality("French"),
        new Nationality("German"),
        new Nationality("Indian"),
        new Nationality("Swiss")
    ]

    constructor(value: string){
        this.value = value;
    }

    static find(nationalityStr: string) : Nationality{
        var retVal : Nationality = new Nationality("unknown")
        Nationality.nationalities.forEach(nationality => {
            if(nationalityStr.toUpperCase() === nationality.value.toUpperCase()){
                retVal = nationality;
            }
        });
        return retVal;
    }
}

export class Gender {
    value: string

    static genders: Gender[] = [
        new Gender("Female"),
        new Gender("Male")
    ]

    constructor(value: string){
        this.value = value;
    }
}

export class Language {
    value: string

    static languages: Language[] = [
        new Language("English"),
        new Language("Deutsch"),
        new Language("Français"),
        new Language("Nederlands")
    ]

    constructor(value: string){
        this.value = value;
    }
}

export class ResidenceStatus {
    value : string
    description: string | null

    static residenceStatusOptions: ResidenceStatus[] = [
        new ResidenceStatus("B", "The right to unlimited stay"),
        new ResidenceStatus("C", "The right to establishment"),
        new ResidenceStatus("D", "EU long-term residency status"),
        new ResidenceStatus("E+", "Right to durable stay for EU nationals"),
        new ResidenceStatus("F+", "Right to durable stay for family members of EU nationals"),
    ]

    constructor(value: string, description: string | null = null) {
        this.value = value;
        this.description = description;
    }

    static find(residenceStatusStr: string):ResidenceStatus{
        var retVal : ResidenceStatus = new ResidenceStatus("unknown", null);
        ResidenceStatus.residenceStatusOptions.forEach(residenceStatus => {
            if(residenceStatusStr.toUpperCase() === residenceStatus.value.toUpperCase()){
                retVal = residenceStatus;
            }
        });
        return retVal;
    }
}

export class IDCard {
    idCardNumber: string
    idCardValidity: Date | null

    constructor(number: string, validity: Date){
        this.idCardNumber = number;
        this.idCardValidity = validity;
    }
}