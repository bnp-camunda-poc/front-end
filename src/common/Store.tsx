export class Store {
    static customerIdKey : string = "customerId";
    static connectionId : string = "connectionId";

    static setItem(key: string, value: string){
        localStorage.setItem(key, value);
    }

    static getItem(key: string){
        return localStorage.getItem(key);
    }

    static removeItem(key: string){
        localStorage.removeItem(key);
    }
}