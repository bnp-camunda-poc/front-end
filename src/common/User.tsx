import { stat } from 'fs';
import moment from 'moment';
import React from 'react';
import { Credentials, Gender, IDCard, Language, Nationality, ResidenceStatus } from './Types';

export class User{
    status: string
    customerId: string

    constructor(status: string, customerId: string){
        this.status = status;
        this.customerId = customerId;
    }
}

export class UserRegistration{
    address: string
    dob: string
    familyName: string
    firstName: string
    email: string
    gender: string
    idCardNumber: string
    idCardValidity: string
    language: string
    nationality: string
    residenceStatus: string

    constructor(credentials: Credentials,
        email: string,
        nationality: Nationality,
        gender: Gender,
        language: Language,
        address: string,
        idCard: IDCard,
        residenceStatus : ResidenceStatus){
        this.address = address;
        this.dob = moment(credentials.dateOfBirth).format("DDMMYYYY");
        this.familyName = credentials.lastName;
        this.firstName = credentials.firstName;
        this.gender = gender.value;
        this.idCardNumber = idCard.idCardNumber;
        this.idCardValidity = moment(idCard.idCardValidity).format("DDMMYYYY");
        this.language = language.value;
        this.nationality = nationality.value;
        this.residenceStatus = residenceStatus.value;
        this.email = email;
    }
  }