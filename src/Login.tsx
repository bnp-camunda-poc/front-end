import React, {Component} from 'react';
import moment from 'moment';
import { Credentials } from './common/Types';
import { Register } from './Register';
import { DataRequest } from './Server';
import { User } from './common/User';
import { Store } from './common/Store';
import { uuidv4 } from './common/Functions';

interface StatusProps {
    loggedOn: boolean,
    setLoginStatus: any
}

interface DisplayState {
    checking: boolean,
    registrationView: boolean
}

interface LoginState {
    credentials: Credentials
}

export class Login extends Component<StatusProps, LoginState> {

    displayState : DisplayState = {
        checking: false,
        registrationView: false
    }

    constructor(props: StatusProps){
        super(props);
        this.state = {
            credentials: new Credentials(
                '',
                '',
                new Date()
            )
        }
        this.displayState.checking = props.loggedOn;
    }

    setFirstName = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        var updateCredentials = this.state.credentials;
        updateCredentials.firstName = value;

        this.setState({ credentials: updateCredentials })
    }
    setLastName = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        var updateCredentials = this.state.credentials;
        updateCredentials.lastName = value;

        this.setState({ credentials: updateCredentials })
    }
    setDateOfBirth = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.valueAsDate;
        var updateCredentials = this.state.credentials;
        updateCredentials.dateOfBirth = value;

        this.setState({ credentials: updateCredentials })
    }

    doSignIn = (response: Response) => {
        response.json().then(data => {
            const user : User = data;
            var loginStatus : boolean = false;

            const checkStatus : string = (process.env.REACT_APP_LOGIN_STATUS || "true");
            console.log(`test : ${JSON.stringify(data)} :: checkstat: ${checkStatus}`);
            if(user.status.toUpperCase() === checkStatus.toUpperCase()){
                Store.setItem(Store.customerIdKey, user.customerId);
                this.displayState.checking = false;
                loginStatus = true;
            } else {
                this.displayState.registrationView = true;
            }

            this.forceUpdate();
            this.props.setLoginStatus(loginStatus);
        });
    }

    checkSignin = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        this.displayState.checking = true;
        this.forceUpdate();

        const dob = moment(this.state.credentials.dateOfBirth).format('DDMMYYYY');
        DataRequest.registerConnectionId(true);

        fetch(DataRequest.requestLogin(this.state.credentials.firstName, this.state.credentials.lastName, dob).url)
        .then(response => this.doSignIn(response))
        .catch(error => {
            this.displayState.registrationView = true;
            this.forceUpdate();
        });
    }

    render() {
        const { firstName, lastName, dateOfBirth } = this.state.credentials;

        const loginDisplay = {
            display: (this.displayState.checking)? 'none' : 'block'
        }
        const checkingDisplay = {
            display: (this.displayState.checking && !this.displayState.registrationView)? 'block' : 'none'
        }
        const registrationDisplay = {
            display: (this.displayState.registrationView)? 'block' : 'none'
        }
        const key: string = Store.getItem(Store.connectionId) || "none";

        return (
            <div>
                <div className="formContainer" style={loginDisplay}>
                    <h2>Login</h2>
                    <form>
                        <label>First Name</label><input type="text" placeholder="First Name" value={firstName} onChange={this.setFirstName}/>
                        
                        <label>Last Name</label><input type="text" placeholder="Last Name" value={lastName} onChange={this.setLastName}/>
                        
                        <label>Date of Birth</label><input type="date" onKeyDown={(e) => { e.preventDefault(); return false; }} value={ moment(dateOfBirth).format('YYYY-MM-DD') } onChange={this.setDateOfBirth}/>

                        <input type="submit" value="Next" onClick={this.checkSignin}/>
                    </form>
                </div>
                <div style={checkingDisplay}>
                    Checking sign in for 
                    <p>'{firstName} {lastName}'</p>
                    <p>born on {dateOfBirth?.toDateString()}</p>
                </div>
                <div style={registrationDisplay}>
                    <Register 
                        credentials={this.state.credentials} 
                        visibility={this.displayState.registrationView} 
                        setVisibility={(visibility: boolean) => {
                                this.displayState.registrationView = visibility;
                                this.displayState.checking = false;
                                this.forceUpdate();
                                console.log(`what be visibility: ${visibility}`)
                            }}
                        setLoginStatus={this.props.setLoginStatus}
                    />
                </div>
                {key}
            </div>
        );
    }

}